# JOURNAL DE BORD DE KNIGHTS ROAD

## *VENDREDI 14 JUIN 2019 - Soutenance et démo*

## *JEUDI 13 JUIN 2019 - Derniers conseils*

## *MERCREDI 12 JUIN 2019 - Réalisation du logiciel final*

## *MARDI 11 JUIN 2019 - Réalisation du logiciel final*

## *VENDREDI 7 JUIN 2019 - Réalisation du logiciel final*

## *JEUDI 6 JUIN 2019 - Réalisation du logiciel final*

## *MERCREDI 5 JUIN 2019 - Les contrats*

## *MARDI 4 JUIN 2019 - Présentation des projets et Contrats*

## *LUNDI 3 JUIN 2019 - Prototypage et Préparation soutenance*

## *MERCREDI 29 MAI 2019 - Prototypage*

#### Nouvelle idée de 2ième monde :

Après conseils de notre tuteur M. Marechal et de M. Perrin et M. Gruber, nous avons décidé de changer notre idée initiale de monde bonus. 

Le nouveau monde bonus apparaîtra avant chaque boss. Il sera vu de profil. Ce monde sera une fenêtre figée composée de :
* quelques plateformes
* quelques monstres qui nous embêteront
* une case bonus à récupérer
* une case qui infligera des dégâts au boss 

Afin de rendre le tout cohérent, une vision du boss prenant les dégâts sera affichée à l'écran après un fondu noir.
Notre héros retournera au monde principal seul et nous ne verrons que le personnage revenir dans ce monde par la porte permettant d'accéder au monde bonus.

Si notre héros venait à être tué dans le monde bonus, aucun dégât ne sera infligé au boss et le bonus ne sera pas récupéré. 

#### Idée de prototypes : 

Après discussion avec notre tuteur hier après-midi et réflexion au sein de l'équipe, nous avions décidé de travailler sur 2 prototypes : 
* la transition entre les deux mondes
* le scrolling de l'écran

Grâce aux conseils de M. Perrin et de M. Gruber nous avons donc décidé de réfléchir aux prototypes suivants : 
* le scrolling de l'écran
* la transition graphique
* commencer à réfléchir aux automates et notamment regarder l'interpréteur 

### INTERPRETEUR 

Tout d'abord nous allons créer un fichier par automate.
Ensuite, nous allons créer un AutomataParser à partir de ces fichiers. 
On applique la fonction run() sur chaque AutomataParser, ce qui nous renverra un Automata sur lequel on appliquera la fonction make(). 

Il faudra ensuite associer à chaque entité son automate. 

#### Questions :

Pour ce long week-end nous avons donc décidé de réfléchir aux questions suivantes : 
* **................**
* **................**
* **................**
* **................**
* **................**
* **................**

## *MARDI 28 MAI 2019 - Conception technique*

### AMPHI

Nous avons présenté notre projet de jeu en Amphi, le seul point à améliorer est le monde bonus.

### CONCEPTION

#### Interaction utilisateur 

Aucune interaction avec la souris 

* Clavier  
    * Flèche du haut : Déplacement vers le haut
    * Flèche de droite : Déplacement vers la droite
    * Flèche de gauche : Déplacement vers la gauche
    * Flèche du bas : Déplacement vers le bas
    * Echap : Pause 
    * Espace : Frapper (pop)
    * A : Bouclier (Wizz)
    * M : Retourner au menu
    * (Optionnel : H pour voir le plus haut score)


#### Structure de données

Entité
*  Personnages
    * Hero
    * Ennemi
        * Orcs
        * Fantomes
        * Squelettes
        * Boss

*  Elements
    * Item
        * Fruit
        * Bottes
        * Parachute
        * Potion
        * Etoile
    * Obstacles
        * Pic
        * Trou
        * Rocher
        * Mur
        * Glue

#### Temps de simulation

Durée du jeu : jusqu'à ce que le joueur perde ou quitte le jeu.

#### Réactions éléments actifs 

* Ennemis 
    * Se dirigent vers le personnage (en diagonale pour tous sauf boss(?) et fantôme)
    * Perdent des PV lorsqu'ils reçoivent un coup
    * Disparaissent du jeu s'ils sont hors de l'écran
    * Subissent les obstacles

* Personnage/Avatar
    * Subit les colisions
    * Bloqué dans l'écran par les boss
    * Ne peut descendre plus bas que l'écran actuel

* Obstacles
    * Trous 
        * Font tomber monstres et joueurs qui vont dedans (les tue)
    * Rochers
        * Bloquent le chemin
    * Murs
        * Bloquent le chemin à toutes les entités, à l'exception des fantômes
        * Coulissent
    * Pics 
        * Infligent des dégâts lorsque qu'une entité marche dessus
    * Glue 
        * Ralenti les entités qui marchent dessus

* Items : réagissent tous au contact de l'avatar UNIQUEMENT
    * Fruit
        * Augmentent les dégâts de l'avatar (durée encore indéfinie)
    * Bottes
        * Bonus de distance (applique un multiplicateur) pour une durée limitée
    * Potion
        * Restaure une partie des PV 
    * Parachute
        * Utilisable une fois lorsque le joueur passe au-dessus d'un trou
    * Etoiles
        * Rend le joueur invinsible pendant une courte période

## *LUNDI 27 MAI 2019 - Conception créative*

Nous avons eu une idée : 

### PRINCIPES DU JEU

Le but du jeu est de parcourir la plus grande distance. Pour cela, le joueur devra avancer dans la map, tuer les ennemis, éviter les obstacles et récupérer les bonus. Pour s'adapter à la difficulté grandissante du jeu, le joueur devra redoubler d'attention. 

### DÉPLACEMENT

*  Sous la limite : 
    * si on appuie sur la flèche du bas, la fenêtre ne bouge plus
    * le héros pourra alors se déplacer dans la partie basse de la fenêtre

*  Sur la limite : 
    * si on appuie sur la flèche de gauche / droite, cela ne déplace pas la fenêtre
    * si on appuie sur la flèche du haut, cela fait avancer notre héros sur la map

### MONDE BONUS

Ce niveau est vu de profil. Il permet de récupérer un bonus tout en évitant / tuant les monstres de ce niveau. Si toutefois le héros se fait tuer dans ce niveau, le héros retourne au jeu principal sans avoir gagné de bonus. 

### ENNEMIS

*  MOBS - 3 types : 
    * Fantôme : intuable (dégât +)
    * Squelette : peut nous frapper, est équipé d'une dagu, tuable (dégât ++)
    * Orc : peut nous frapper, est mains nues, tuable (dégat +++)

Les mobs sont tous contournables

*  BOSS :
    * bloque la fenêtre
    * obligation de le tuer pour avancer dans le jeu
    * si on le tue, la difficulté et la vie augmentent
    * sinon, GAME OVER

### OBSTACLES

* Rocher : à contourner
* Pic : enlève de la vie
* Trou : mortel 
* Glue : ralenti

### ITEMS

* Potion : rend de la vie
* Étoile : rend invincible
* Parachute : empêche de tomber
* Bottes : score augmente 2 fois plus vite
* Fruit : dégats x2

### IDEES DE PROTOTYPES

*  Défilement du paysage
*  Changement de monde (Monde normal <-> Monde bonus)
*  Limite de déplacement du personnage
*  Apparition aléatoire des items / mobs