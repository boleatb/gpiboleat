/*
 * Educational software for a basic game development
 * Copyright (C) 2018  Pr. Olivier Gruber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package edu.ricm3.game.sample;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Random;

import javax.imageio.ImageIO;

import edu.ricm3.game.GameModel;

public class Model extends GameModel {
  
	BufferedImage tileset;
	Tileset m_tileset;
	int code;

  public Model() {
    loadSprites();
    m_tileset = new Tileset(tileset,5,25);
    
  }
  
  
  
  @Override
  public void shutdown() {
    
  }


  
  
  /**
   * Simulation step.
   * 
   * @param now
   *          is the current time in milliseconds.
   */
  @Override
  public void step(long now) {
	 long elapsed = now - m_tileset.m_lastMove;
	 if (elapsed > 60L) {
		 switch (code) {
		 case 0x26 :
			 m_tileset.scroll();
			 code=0;
			 break;
		 case 0x20 :
				 m_tileset.black();
				 if (m_tileset.bdone) {
					 code = 0;
				 } else {
					 code = 0x20;
				 }
				 break;
		 }
	 }
  }

  private void loadSprites() {
    /*
     * Cowboy with rifle, western style; png; 48x48 px sprite size
     * Krasi Wasilev ( http://freegameassets.blogspot.com)
     */
    File imageFile = new File("game.sample/sprites/tileset.png");
    try {
      tileset = ImageIO.read(imageFile);
    } catch (IOException ex) {
      ex.printStackTrace();
      System.exit(-1);
    }
   
  }

}
