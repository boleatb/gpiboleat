package edu.ricm3.game.sample;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;

public class Tileset {
	
	int s_w,s_h;
	BufferedImage m_spritesheet;
	BufferedImage[] m_sprites;
	int m_nrows, m_ncols;
	int m_w, m_h;
	int[][] tabimages;
	int nbcol;
	int nblig;
	long m_lastMove;
	int nolig,nocol;
	boolean bdone;
	boolean init;
	
	Tileset(BufferedImage sprite, int rows, int columns){
		m_spritesheet=sprite;
		m_ncols = columns;
		m_nrows = rows;
		bdone=false;
		init = false;
		splitSprite();
	}
	
	void splitSprite() {
		int width = m_spritesheet.getWidth(null);
		int height = m_spritesheet.getHeight(null);
		m_sprites = new BufferedImage[m_nrows * m_ncols];
		m_w = width / m_ncols;
		m_h = height / m_nrows;
		for (int i = 0; i < m_nrows; i++) {
			for (int j = 0; j < m_ncols; j++) {
				int x = j * m_w;
				int y = i * m_h;
				m_sprites[(i * m_ncols) + j] = m_spritesheet.getSubimage(x, y, m_w, m_h);
			}
		}
	}
	
	void setTab() {
		nbcol=s_w/m_w+1;
		nblig=s_h/m_h+1;
		System.out.println(nbcol);
		tabimages = new int[nblig][nbcol];
		for (int i=0;i<nblig;i++) {
			for (int j=0;j<nbcol;j++) {
				if(j < 4 || j>nbcol-6) {
					tabimages[i][j]=93;
					
				} else if(j == 4 ) {
					tabimages[i][j]=35;
				}else if (j == nbcol-6){
					tabimages[i][j]=33;
				}else {
					if((i%2==0)) {
						tabimages[i][j]=102;
					} else {
						tabimages[i][j]=105;
					}
				}
			}
		}
		init = true;
	}
	
	void scroll() {
		for (int i=0;i<nblig;i++) {
			for (int j=0;j<nbcol;j++) {
				if(tabimages[i][j]==33) {
					tabimages[i][j]=32;
				}
				else if(tabimages[i][j]==32) {
					tabimages[i][j]=33;
				}
				else if(tabimages[i][j]==35) {
					tabimages[i][j]=36;
				}
				else if(tabimages[i][j]==36) {
					tabimages[i][j]=35;
				} 
				else if(tabimages[i][j]==102) {
					tabimages[i][j]=105;
				}
				else if(tabimages[i][j]==105) {
					tabimages[i][j]=102;
				}
			}
		}
	}
	
	void black() {
		tabimages[nolig][nocol]= 6;
		nocol++;
		if ( nocol == nbcol) {
			nolig++;
			nocol=0;
		}
		if((nolig == nblig-1) && (nocol == nbcol-1)) {
			bdone = true;
		}
	}
	
	void kcalb() {
		
		nocol--;
		if((nolig == 0) && (nocol == 0)) {
			bdone = true;
		}
		if ( nocol == 0) {
			nolig--;
			nocol=nbcol-1;
		}
	}
	
	void paint(Graphics g) {
		for (int i=0;i<nblig;i++) {
			for (int j=0;j<nbcol;j++) {
				Image img = m_sprites[tabimages[i][j]];
				g.drawImage(img,m_w*j, m_h*i,null);
			}
		}
	}

}
