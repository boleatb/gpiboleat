# Knight's Road 

## Principe

Knight Road est un jeu à 1 joueur qui consiste à parcourir un maximum de distance sans se faire tuer par des ennemis. Le monde est infini à la verticale mais fini à l'horizontale.
Tout le long de son parcours, le joueur pourra récupérer différents Items pour l'aider. 
Il devra affronter des mobs et éviter les obstacles pour atteindre les Boss qui lui permettront d'accéder à un niveau supérieur.
Avant chaque Boss, il aura la possibilité de l'affaiblir en rentrant dans un monde parallèle. Celui ci lui permettra de débloquer un item et une aide qui blessera le boss.

#### Temps de simulation

Durée du jeu : jusqu'à ce que le joueur perde ou quitte le jeu.

#### Mode de jeu

Aucune interaction avec la souris 

* Clavier  
    * Flèche du haut : Déplacement vers le haut
    * Flèche de droite : Déplacement vers la droite
    * Flèche de gauche : Déplacement vers la gauche
    * Flèche du bas : Déplacement vers le bas
    * Echap : Pause 
    * Espace : Frapper (pop)
    * A : Bouclier (Wizz)
    * M : Retourner au menu
    * (Optionnel : H pour voir le plus haut score)


#### Structure de données

*  Personnages
    * Hero
    * Ennemi
        * Orcs
        * Fantomes
        * Squelettes
        * Boss
*  Elements
    * Item
        * Fruit
        * Bottes
        * Parachute
        * Potion
        * Etoile
    * Obstacles
        * Pic
        * Trou
        * Rocher
        * Mur
        * Glue

#### Réactions éléments actifs 

* Ennemis 
    * Se dirigent vers le personnage 
    * Perdent des PV lorsqu'ils reçoivent un coup
    * Disparaissent du jeu s'ils sont hors de l'écran
    * Subissent les obstacles
* Personnage/Avatar
    * Subit les colisions
    * Bloqué dans l'écran par les boss
    * Ne peut descendre plus bas que l'écran actuel
* Obstacles
    * Trous 
        * Font tomber monstres et joueurs qui vont dedans (les tue)
    * Rochers
        * Bloquent le chemin
    * Murs
        * Bloquent le chemin à toutes les entités, à l'exception des fantômes
        * Coulissent
    * Pics 
        * Infligent des dégâts lorsque qu'une entité marche dessus
    * Glue 
        * Ralenti les entités qui marchent dessus
* Items : Réagissent tous au contact de l'avatar UNIQUEMENT
    * Fruit
        * Augmentent les dégâts de l'avatar (durée encore indéfinie)
    * Bottes
        * Bonus de distance (applique un multiplicateur) pour une durée limitée
    * Potion
        * Restaure une partie des PV 
    * Parachute
        * Utilisable une fois lorsque le joueur passe au-dessus d'un trou
    * Etoiles
        * Rend le joueur invinsible pendant une courte période

#### Monde parallèle 

Avant chaque boss, un niveau bonus apparaîtra : ce monde sera un jeu de plateforme vu de profil, dans une fenêtre figée.

Dans ce monde, le héros pourra récupérer un bonus et pourra appuyer sur une case qui permettra d'infliger des dégâts au boss. 

Des monstres apparaîtront afin de nous compliquer la tâche. 

Si le héros se fait tuer dans ce monde, il reviendra au monde principal sans bonus, ni dégâts infligés au boss. 

En revanche, si le héros réussi à récupérer le bonus et à appuyer sur la case qui inflige des dégâts au boss, une animation apparaîtra à l'écran : on y verra le boss prendre des dégats et notre héros revenir par la porte du 2ième monde. 